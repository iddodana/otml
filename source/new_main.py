import codecs
from pathlib import Path
from otml_configuration_manager import OtmlConfigurationManager
from grammar.lexicon import Lexicon
from grammar.feature_table import FeatureTable
from grammar.constraint_set import ConstraintSet
from grammar.grammar import Grammar
from traversable_grammar_hypothesis import TraversableGrammarHypothesis
from corpus import Corpus
from simulated_annealing import SimulatedAnnealing


current_file_path = Path(__file__)

source_directory_path = current_file_path.parent

configuration_file_path = (
    f"{source_directory_path}/tests/fixtures/configuration/otml_configuration.json"
)

configuration_json_str = codecs.open(configuration_file_path, "r").read()
OtmlConfigurationManager(configuration_json_str)

# feature_table_file_path = f"{source_directory_path}/tests/fixtures/feature_table/french_deletion_feature_table.json"
# corpus_file_path = f"{source_directory_path}/tests/fixtures/corpora/french_deletion_corpus.txt"
# constraint_set_file_path = f"{source_directory_path}/tests/fixtures/constraint_sets/french_deletion_constraint_set.json"

# to delete
feature_table_file_path = f"{source_directory_path}/tests/fixtures/feature_table/a_b_and_cons_feature_table.json"
corpus_file_path = f"{source_directory_path}/tests/fixtures/corpora/bb_corpus.txt"
constraint_set_file_path = (
    f"{source_directory_path}/tests/fixtures/constraint_sets/lior_test_faith.json"
)

configuration_json_str = codecs.open(configuration_file_path, "r").read()
OtmlConfigurationManager(configuration_json_str)


feature_table = FeatureTable.load(feature_table_file_path)
corpus = Corpus.load(corpus_file_path)
constraint_set = ConstraintSet.load(constraint_set_file_path, feature_table)
lexicon = Lexicon(corpus.get_words(), feature_table)
grammar = Grammar(feature_table, constraint_set, lexicon)
data = corpus.get_words()
traversable_hypothesis = TraversableGrammarHypothesis(grammar, data)
simulated_annealing = SimulatedAnnealing(traversable_hypothesis)
simulated_annealing.run()

print("done")
