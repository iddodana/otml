class UnicodeMixin(object):
    __str__ = lambda x: x.__unicode__()

    __repr__ = __str__
