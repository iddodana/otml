import codecs
from pathlib import Path

from debug_tools import write_to_mermaid
from otml_configuration_manager import OtmlConfigurationManager

current_file_path = Path(__file__)

source_directory_path = current_file_path.parent
research_directory_path = source_directory_path.parent

configuration_file_path = (
    f"{research_directory_path}/source/tests/fixtures/configuration/otml_configuration.json"
)

configuration_file_path = f"{research_directory_path}/source/tests/fixtures/configuration/otml_configuration.json"

configuration_json_str = codecs.open(configuration_file_path, "r").read()
OtmlConfigurationManager(configuration_json_str)

from corpus import Corpus
from grammar.grammar import Grammar
from grammar.lexicon import Lexicon, Word
from grammar.constraint_set import ConstraintSet
from grammar.feature_table import FeatureTable

constraint_set_file_path = f"{research_directory_path}/source/tests/fixtures/constraint_sets/bb_son_with_end_delete_b#_constraint_set.json"
feature_table_file_path = f"{research_directory_path}/source/tests/fixtures/feature_table/a_b_and_son_with_end_and_trace_feature_table.json"

#constraint_set_file_path = f"{research_directory_path}/source/tests/fixtures/constraint_sets/catalan_without_end_constraint_set.json"
#feature_table_file_path = f"{research_directory_path}/source/tests/fixtures/feature_table/catalan_without_end_feature_table.json"

feature_table = FeatureTable.load(feature_table_file_path)
constraint_set = ConstraintSet.load(constraint_set_file_path, feature_table)

for constraint in constraint_set.constraints:
    write_to_mermaid(constraint.get_transducer(),  constraint.get_constraint_name())


lexicon = Lexicon([], feature_table)

grammar = Grammar(feature_table, constraint_set, lexicon)

bb_word = Word("ab#", feature_table)

print(grammar._get_outputs(bb_word))
print("done")

